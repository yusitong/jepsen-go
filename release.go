package main

type Release string

const CentOSTag Release = "centos"
const UbuntuTag Release = "ubuntu"
const DebianTag Release = "debian"
