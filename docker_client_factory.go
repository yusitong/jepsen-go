package main

import (
	"github.com/docker/docker/client"
)

const DEFAULT_PROPERTIES_FILE_NAME = "docker-java.properties"

type DockerClientFactory struct {
}

//type DockerClientConfig struct {
//	host string
//}
//var dockerClientConfig DockerClientConfig

func init() {
	// TODO load docker client config from file

}

func (dcf DockerClientFactory) getDockerClient() *client.Client {
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		panic(err)
	}
	return cli
}
