package main

import (
	"math/rand"
	"strconv"
	"strings"
	"time"
)

// Is a string blank?
func isBlank(s string) bool {
	return len(strings.TrimSpace(s)) == 0
}
func MustAToI(s string) int {
	i, err := strconv.Atoi(s)
	if err != nil {
		panic(err)
	}
	return i
}

func NewRand() *rand.Rand {
	return rand.New(rand.NewSource(time.Now().UnixNano()))
}
