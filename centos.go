package main

import (
	"bytes"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"log"
)

// CentOS docker centos client
type CentOS struct {
	Container    types.Container
	DockerClient *client.Client
}

func (c CentOS) init() {
	// yum change the resource
	c.rmRecursiveForce("/etc/yum.repos.d/*.repo")
	// ATTENTION URL change because of CentOS 8 End Of Life
	// https://help.aliyun.com/document_detail/405635.html
	c.curlDownload("/etc/yum.repos.d/Centos-vault-8.5.2111.repo",
		"https://mirrors.aliyun.com/repo/Centos-vault-8.5.2111.repo")
	c.curlDownload("/etc/yum.repos.d/epel-archive-8.repo",
		"https://mirrors.aliyun.com/repo/epel-archive-8.repo")
	if _, err := c.exec("sed", "-i", "s/mirrors.cloud.aliyuncs.com/mirrors.aliyun.com/g",
		"/etc/yum.repos.d/Centos-vault-8.5.2111.repo"); err != nil {
		panic(err)
	}

	if _, err := c.exec("yum", "clean", "all"); err != nil {
		panic(err)
	}
	if _, err := c.exec("yum", "makecache"); err != nil {
		panic(err)
	}
	//if err := c.exec("yum", "-y", "update"); err != nil {
	//	panic(err)
	//}
	// install basic package
	for _, pkg := range []string{"wget", "net-tools", "iptables-services"} {
		c.installWithPkgManger(pkg)
	}
}

func (c CentOS) execBackground(cmd ...string) {
	log.Println("execBackground: ", cmd)
	containerID := c.Container.ID
	execConfig := types.ExecConfig{
		User: "root",
		Cmd:  cmd,
	}
	idResponse, err := c.DockerClient.ContainerExecCreate(defaultCtx, containerID, execConfig)
	if err != nil {
		panic(err)
	}
	execStartCheck := types.ExecStartCheck{}
	err = c.DockerClient.ContainerExecStart(defaultCtx, idResponse.ID, execStartCheck)
	if err != nil {
		panic(err)
	}
}

func (c CentOS) exec(cmd ...string) (string, error) {
	log.Println("exec: ", cmd)
	containerID := c.Container.ID
	execConfig := types.ExecConfig{
		User:         "root",
		AttachStderr: true,
		AttachStdout: true,
		Cmd:          cmd,
	}
	idResponse, err := c.DockerClient.ContainerExecCreate(defaultCtx, containerID, execConfig)
	if err != nil {
		panic(err)
	}
	execStartCheck := types.ExecStartCheck{}
	resp, err := c.DockerClient.ContainerExecAttach(defaultCtx, idResponse.ID, execStartCheck)
	if err != nil {
		return "", err
	}
	buf := new(bytes.Buffer)
	if _, err := buf.ReadFrom(resp.Reader); err != nil {
		return "", err
	}
	return buf.String(), err
}

func (c CentOS) cp(source string, target string) {
	c.exec("cp", "-r", source, target)
}

func (c CentOS) mkdir(dirPath string) {
	c.exec("mkdir", "-p", dirPath)
}

func (c CentOS) touch(path string) {
	c.exec("touch", path)
}

func (c CentOS) rmRecursiveForce(filePattern string) {
	// TODO bashExec different with exec?
	if err := c.bashExec("rm -rf " + filePattern); err != nil {
		panic(err)
	}
}

func (c CentOS) fileWrite(filePath string, content string) {
	c.bashExec("echo " + content + " > " + filePath)
}

func (c CentOS) fileAppendLine(filePath string, content string) {
	c.exec("sed", "-i", "$a\\"+content, filePath)
}

func (c CentOS) getIP() string {
	ip, err := c.exec("hostname", "-I")
	if err != nil {
		panic(err)
	}
	return ip
}

func (c CentOS) wget(localPath string, url string) {
	log.Println("downloading resource from: " + url)
	c.exec("wget", "-O", localPath, url)
}

func (c CentOS) curlDownload(localPath, url string) {
	c.exec("curl", "-o", localPath, url)
}

func (c CentOS) untargz(tarFile string, targetDir string) {
	c.exec("tar", "zxf", tarFile, "-C", targetDir, "--strip-components=1")
}

func (c CentOS) installJDK8() {
	c.installWithPkgManger("java-1.8.0-openjdk-devel.x86_64")
}

func (c CentOS) installWithPkgManger(pkgName string) {
	log.Println("Start install [" + pkgName + "] with yum")
	if _, err := c.exec("yum", "install", "-y", pkgName); err != nil {
		panic(err)
	}
}

func (c CentOS) bashExec(command string) error {
	_, err := c.exec("bash", "-c", command)
	return err
}

func (c CentOS) ls(command string) string {
	s, err := c.exec("ls", command)
	if err != nil {
		panic(err)
	}
	//log.Println("first 20:", []rune(s)[8:])
	// TODO unknown character at start?
	return string([]rune(s)[8:])
}
