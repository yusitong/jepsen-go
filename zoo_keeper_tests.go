package main

import (
	"errors"
	"fmt"
	"github.com/anishathalye/porcupine"
	"log"
	"time"
)

func runZooKeeperTests(host string, clusterName string, node int, image string, op int) {
	release := CentOSTag
	tag := "8"
	if image == string(UbuntuTag) {
		//release = Release.Ubuntu
		//tag = "20.04"
		log.Fatalln("Ubuntu is not support currently, use centos instead.")
	}
	// cluster
	cluster := NewCluster(clusterName)
	var clients = make([]Client, node)
	var ports = make([]int, node)
	for i := 1; i <= node; i++ {
		var port = 2180 + i
		log.Printf("startup no.%d node\n", i)
		zk := Application(NewZooKeeper("3.6.1", node, i))
		nodeBuilder := NodeBuilder{
			cluster:           &cluster,
			osRelease:         release,
			imageTag:          tag,
			name:              fmt.Sprintf("n%d", i),
			portBindingsStr:   fmt.Sprintf("%d:2181/tcp", port),
			volumeBindingsStr: "",
			envVariablesStr:   "",
			application:       &zk,
		}
		_ = nodeBuilder.build()
		ports[i-1] = port
	}
	for i := 0; i < node; i++ {
		clients[i] = NewZKClient(host, ports[i])
	}
	log.Println("set generator")
	generator, _ := NewGenerator(clients...)
	log.Println("generator gen recordList")
	recordList := generator.gen(op, "Read", "Write")
	log.Println("generator gen recordList finished")

	var history []porcupine.Event
	var renumber = make(map[int]int)
	var id = 0
	var recordOrders = make(map[string]int)
	for i, record := range recordList {
		isRead := record.func_ == "read"
		var value RegisterValue
		switch record.type_ {
		case INVOKE:
			if isRead {
				value = RegisterValue{true, true, 0}
			} else {
				value = RegisterValue{true, false, MustAToI(record.args[0])}
			}
			renumber[i] = id
			history = append(history, porcupine.Event{
				ClientId: record.pid,
				Kind:     porcupine.CallEvent,
				Value:    value,
				Id:       id,
			})
			recordOrders[fmt.Sprintf("%#v", record)] = id
			id++
		case OK:
			if isRead {
				value = RegisterValue{false, true, MustAToI(record.args[0])}
			} else {
				value = RegisterValue{false, false, 0}
			}
			history = append(history, porcupine.Event{
				ClientId: record.pid,
				Kind:     porcupine.ReturnEvent,
				Value:    value,
				Id:       recordOrders[fmt.Sprintf("%#v", *record.prev)],
			})
		default:
			panic(errors.New("zk record type error"))
		}
	}
	log.Printf("finish history count %#v", history)

	registerModelInit := func() interface{} {
		return RegisterState{val: 0}
	}
	registerModelStep := func(stateInter interface{}, inputInter interface{}, outputInter interface{}) (bool, interface{}) {
		oldState := stateInter.(RegisterState)
		input := inputInter.(RegisterValue)
		output := outputInter.(RegisterValue)
		if input.isRead {
			if output.val != oldState.val {
				return false, nil
			}
			return true, RegisterState{val: oldState.val}
		} else {
			return true, RegisterState{val: input.val}
		}
	}
	casRegisterModel := porcupine.Model{
		Partition:         nil,
		PartitionEvent:    nil,
		Init:              registerModelInit,
		Step:              registerModelStep,
		Equal:             nil,
		DescribeOperation: nil,
		DescribeState:     nil,
	}
	checkResult, linearizationInfo := porcupine.CheckEventsVerbose(casRegisterModel, history, 5*time.Minute)
	fmt.Println("checkResult: ", checkResult)
	log.Printf("linearizationInfo: %+v\n", linearizationInfo)
	if err := porcupine.VisualizePath(casRegisterModel, linearizationInfo, "zk_result.html"); err != nil {
		panic(err)
	}
}
