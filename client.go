package main

type Client interface {
	// Returns a client which is ready to accept operations via invoke!
	// Open *should not* affect the logical state of the test;
	// it should not, for instance, modify tables or insert records.
	open()
	// Called to set up database state for testing.
	setup()
	// Apply an operation to the client,
	// returning an operation to be appended to the history.
	invoke(record Record) Record
	// Tear down database state when work is complete.
	teardown()
	// Close the client connection when work is completed or an invocation crashes the client.
	// Close should not affect the logical state of the test.
	close()
}
