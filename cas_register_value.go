package main

import (
	"fmt"
	"log"
)

type CASRegisterValueOPType int

const (
	CASRegisterValueOP_R = CASRegisterValueOPType(iota)
	CASRegisterValueOP_W
	CASRegisterValueOP_C
)

type CASRegisterValue struct {
	isCall        bool
	op            CASRegisterValueOPType
	in1, in2, out int
}

func (v CASRegisterValue) String() string {
	switch v.op {
	case CASRegisterValueOP_R:
		if v.isCall {
			return "read"
		} else {
			return fmt.Sprint(v.out)
		}
	case CASRegisterValueOP_W:
		if v.isCall {
			return fmt.Sprintf("write(%d)", v.in1)
		} else {
			return " "
		}
	case CASRegisterValueOP_C:
		if v.isCall {
			return fmt.Sprintf("cas(%d,%d)", v.in1, v.in2)
		} else {
			return fmt.Sprint(v.out)
		}
	}
	log.Println("func (v CASRegisterValue) String() empty")
	return ""
}
