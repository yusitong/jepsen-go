module jepsen-go

go 1.16

require (
	github.com/anishathalye/porcupine v0.1.2
	github.com/containerd/containerd v1.5.9 // indirect
	github.com/docker/docker v20.10.12+incompatible
	github.com/docker/go-connections v0.4.0
	github.com/go-zookeeper/zk v1.0.2
	github.com/morikuni/aec v1.0.0 // indirect
	github.com/opencontainers/image-spec v1.0.2
	go.etcd.io/etcd/client/v3 v3.5.2
	google.golang.org/grpc v1.44.0 // indirect
)
