package main

type OS interface {
	init()
	exec(cmd ...string) (string, error)
	execBackground(cmd ...string)

	cp(source string, target string)
	mkdir(dirPath string)
	touch(path string)
	rmRecursiveForce(filePattern string)

	fileWrite(filePath string, content string)
	fileAppendLine(filePath string, content string)

	getIP() string
	wget(localPath string, url string)
	curlDownload(localPath, url string)
	untargz(tarFile string, targetDir string)
	installJDK8()
}
