package main

import (
	"context"
	"errors"
	"fmt"
	"go.etcd.io/etcd/client/v3"
	"log"
	"math/rand"
	"time"
)

type EtcdClient struct {
	host     string
	port     int
	testKey  string
	rand     *rand.Rand
	client   *clientv3.Client
	kvClient clientv3.KV
}

type EtcdClientPointer struct {
	*EtcdClient
}

func NewEtcdClient(host string, port int) EtcdClientPointer {
	return EtcdClientPointer{&EtcdClient{
		host:     host,
		port:     port,
		testKey:  "4-test",
		rand:     NewRand(),
		client:   nil,
		kvClient: nil,
	}}
}

func (e EtcdClientPointer) open() {
	if e.EtcdClient == nil {
		panic(errors.New("EtcdClientPointer this is nil"))
	}
	cli, err := clientv3.New(clientv3.Config{
		Endpoints:   []string{fmt.Sprintf("http://%s:%d", e.host, e.port)},
		DialTimeout: 2 * time.Second,
	})
	if err != nil {
		panic(err)
	}
	e.client = cli
	e.kvClient = clientv3.NewKV(cli)
}

// setup data structure initial value
func (e EtcdClientPointer) setup() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
	log.Printf("before put\n")
	resp, err := e.kvClient.Put(ctx, e.testKey, "0")
	log.Printf("after put\n")
	cancel()
	if err != nil {
		panic(err)
	}
	log.Printf("%#v\n", resp)
}

func (e EtcdClientPointer) invoke(record Record) Record {
	switch record.func_ {
	case "read":
		response, err := e.kvClient.Get(context.Background(), e.testKey)
		if err != nil {
			panic(err)
		}
		res := string(response.Kvs[0].Value)
		return record.Ok(res)
	case "write":
		newValue := record.args[0]
		e.kvClient.Put(context.Background(), e.testKey, newValue)
		return record.Ok()
	case "cas":
		oldValue := record.args[0]
		newValue := record.args[1]
		response, err := e.kvClient.Txn(context.Background()).
			If(clientv3.Compare(clientv3.Value(e.testKey), "=", oldValue)).
			Then(clientv3.OpPut(e.testKey, newValue), clientv3.OpGet(e.testKey)).
			Else(clientv3.OpGet(e.testKey)).Commit()
		if err != nil {
			panic(err)
		}
		if response.Succeeded {
			return record.Ok(string(response.Responses[1].
				GetResponseRange().GetKvs()[0].Value))
		} else {
			return record.Ok(string(response.Responses[0].
				GetResponseRange().GetKvs()[0].Value))
		}
	}
	panic(errors.New("EtcdClientPointer invoke func error"))
}

func (e EtcdClientPointer) teardown() {
	_, err := e.kvClient.Delete(context.Background(), e.testKey)
	if err != nil {
		panic(err)
	}
}

func (e EtcdClientPointer) close() {
	err := e.client.Close()
	if err != nil {
		panic(err)
	}
}

// Operation

func (e EtcdClientPointer) OperationRead() Record {
	return NewRecordWithFuncAndArgs("read")
}
func (e EtcdClientPointer) OperationWrite() Record {
	x := fmt.Sprint(e.rand.Intn(5))
	return NewRecordWithFuncAndArgs("write", x)
}
func (e EtcdClientPointer) OperationCAS() Record {
	a := fmt.Sprint(e.rand.Intn(5))
	b := fmt.Sprint(e.rand.Intn(5))
	return NewRecordWithFuncAndArgs("cas", a, b)
}
