package main

import (
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"log"
)

type OSFactory struct {
}

func (o OSFactory) getContainerOS(release Release, container types.Container, dockerClient *client.Client) OS {
	var instance OS
	switch release {
	//case DebianTag:
	//case Ubuntu:
	case CentOSTag:
		fallthrough
	default:
		instance = CentOS{container, dockerClient}
		log.Println("create CentOS instance")
	}
	instance.init()
	return instance
}
