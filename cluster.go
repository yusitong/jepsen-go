package main

import (
	"errors"
	"fmt"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/network"
	"github.com/docker/docker/api/types/strslice"
	"github.com/docker/go-connections/nat"
	"log"
	"strconv"
	"strings"

	"context"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"
)

var clusterMap = make(map[string]Cluster)
var defaultNetName = "bridge"
var defaultNet types.NetworkResource
var dockerClientFactory DockerClientFactory
var globalDockerClient *client.Client
var osFactory OSFactory

// Cluster the cluster for test
type Cluster struct {
	clusterName  string
	dockerClient *client.Client
	network      types.NetworkResource
	nodeMap      map[string]Node
}

var defaultCtx context.Context

func init() {
	defaultCtx = context.Background()
	globalDockerClient = dockerClientFactory.getDockerClient()
	networkListOptions := types.NetworkListOptions{Filters: filters.NewArgs(
		filters.KeyValuePair{
			Key:   "name",
			Value: defaultNetName,
		},
	)}
	networks, err := globalDockerClient.NetworkList(defaultCtx, networkListOptions)
	if len(networks) != 1 {
		if err == nil {
			err = errors.New("exist other networks with same name")
		}
		panic(errors.New("failed to create global Jepsen network " +
			defaultNetName + " : " + err.Error()))
	}
	defaultNet = networks[0]
}

// NewCluster remove old network and containers whose name may conflict, and create new network
func NewCluster(name string) (this Cluster) {
	var err error
	this = Cluster{
		dockerClient: dockerClientFactory.getDockerClient(),
		nodeMap:      make(map[string]Node),
	}

	// ensure not exist cluster with same name
	if _, ok := clusterMap[name]; ok {
		log.Fatalln("There has been a cluster with the same name: [" + name + "]")
	}
	this.clusterName = name

	// remove container with conflict name
	for i := 0; i < 15; i++ {
		this.removeContainerByName("n" + strconv.Itoa(i))
	}

	// remove all networks with conflict name
	log.Println("Remove exists networks: [" + this.clusterName + "]")
	networkListOptions := types.NetworkListOptions{
		Filters: filters.NewArgs(
			filters.KeyValuePair{
				Key:   "name",
				Value: this.clusterName,
			},
		),
	}
	networkResources, _ := this.dockerClient.NetworkList(defaultCtx, networkListOptions)
	for _, networkResource := range networkResources {
		if err := this.dockerClient.NetworkRemove(defaultCtx, networkResource.ID); err != nil {
			panic(err)
		}
	}

	// create network
	networkCreate := types.NetworkCreate{
		CheckDuplicate: false,
		Driver:         "",
		Scope:          "",
		EnableIPv6:     false,
		IPAM:           nil,
		Internal:       false,
		Attachable:     false,
		Ingress:        false,
		ConfigOnly:     false,
		ConfigFrom:     nil,
		Options:        nil,
		Labels:         nil,
	}
	createResponse, err := this.dockerClient.NetworkCreate(defaultCtx, this.clusterName, networkCreate)
	if createResponse.Warning != "" {
		log.Println("create network warning: ", createResponse.Warning)
	}
	if err != nil {
		panic(errors.New("failed to create global jepsen network" + err.Error()))
	}

	// get created network
	netWorkListOptions := types.NetworkListOptions{
		Filters: filters.NewArgs(
			filters.KeyValuePair{
				Key:   "name",
				Value: this.clusterName,
			}),
	}
	networkResources, err = this.dockerClient.NetworkList(defaultCtx, netWorkListOptions)
	if len(networkResources) != 1 {
		if err == nil {
			err = errors.New("exist other networks with same name")
		}
		panic(errors.New("failed to create global Jepsen network " +
			this.clusterName + " : " + err.Error()))
	}
	this.network = networkResources[0]

	// insert now cluster into cluster map
	clusterMap[this.clusterName] = this
	log.Println("Create a new cluster with network: " + this.network.Name)
	return
}

func (c *Cluster) removeContainerByName(name string) {
	cli := c.dockerClient
	var err error
	containerListOptions := types.ContainerListOptions{
		All: true,
		Filters: filters.NewArgs(
			filters.KeyValuePair{
				Key:   "name",
				Value: name,
			},
		),
	}
	existContainersWithSameName, err := cli.ContainerList(defaultCtx, containerListOptions)
	for i, con := range existContainersWithSameName {
		existContainerId := con.ID
		log.Printf("stop and remove container with name %s, %d of %d\n",
			name, i+1, len(existContainersWithSameName))
		log.Println("stop container")
		err = cli.ContainerStop(defaultCtx, existContainerId, nil)
		if err != nil {
			panic(err)
		}
		log.Println("remove container")
		// TODO container remove options?
		err = cli.ContainerRemove(defaultCtx, existContainerId, types.ContainerRemoveOptions{})
		if err != nil {
			panic(err)
		}
	}
}

type NodeBuilder struct {
	cluster           *Cluster
	osRelease         Release
	imageTag          string
	name              string
	portBindingsStr   string
	volumeBindingsStr string
	envVariablesStr   string
	application       *Application
}

type Node struct {
	name        string
	ip          string
	os          OS
	container   types.Container
	application Application
	netDelayVal int
	netDelayEps int
}

func NewNode(name string, os OS, created types.Container, application Application) Node {
	return Node{
		name:        name,
		ip:          os.getIP(),
		os:          os,
		container:   created,
		application: application,
	}
}

func aToUint16(s string) uint16 {
	resInt, err := strconv.Atoi(s)
	if err != nil {
		panic(err)
	}
	return uint16(resInt)
}

func (nb *NodeBuilder) multiBuild(wantedNodeCount int) (res []Node) {
	if wantedNodeCount <= 0 {
		return
	}
	if nb.application == nil {
		panic(errors.New("A specified application is need, get null"))
	}
	if isBlank(nb.name) {
		panic(errors.New("A specified application is need, get null"))
	}
	if nb.osRelease == "" {
		nb.osRelease = CentOSTag
	}
	if isBlank(nb.imageTag) {
		nb.imageTag = "latest"
	}
	imageName := string(nb.osRelease) + ":" + nb.imageTag
	if !nb.ensureImageExists(imageName) {
		panic(errors.New("The image needed does not exist and failed pull it from registry."))
	}
	log.Printf("Want to create container[%s] with image[%s] and port[%s] and volume[%s] and env[%s]",
		nb.name, imageName, nb.portBindingsStr, nb.volumeBindingsStr, nb.envVariablesStr)

	var portBindings = strings.Split(nb.portBindingsStr, ",")
	// TODO volume
	// var volumeBindings = strings.Split(nb.volumeBindingsStr, ",")
	//var volumeBindings []string
	var envVariables = strings.Split(nb.envVariablesStr, "\\\\,")
	log.Printf("Env list= %v", envVariables)

	// bind container port
	hasPortBindings := false
	var portMaps = make(nat.PortMap)
	var exposedPorts = make(nat.PortSet)
	if len(portBindings) != 0 {
		for _, port := range portBindings {
			portArray := strings.Split(port, ":")
			log.Println("portArray ", portArray)
			// TODO real ip?
			portMaps[nat.Port(portArray[1])] = []nat.PortBinding{
				{HostIP: "0.0.0.0", HostPort: portArray[0]},
			}
			exposedPorts[nat.Port(portArray[1])] = struct{}{}
			hasPortBindings = true
		}
	}

	//hasVolumeBindings := false

	hostConfig := container.HostConfig{
		RestartPolicy: container.RestartPolicy{}, // TODO empty is noRestart?
		Privileged:    true,
	}
	if hasPortBindings {
		hostConfig.PortBindings = portMaps
	}
	//if hasVolumeBindings{
	//}

	// environment variable
	var envList []string
	for _, env := range envVariables {
		envArray := strings.Split(env, "=")
		if len(envArray) == 2 && !isBlank(envArray[1]) {
			envList = append(envList, strings.TrimSpace(envArray[0])+"="+strings.TrimSpace(envArray[1]))
		}
	}

	//  create nodes in the cluster
	for i := 1; i <= wantedNodeCount; i++ {
		nodeName := fmt.Sprintf("%s%d", nb.name, i)
		if wantedNodeCount == 1 {
			nodeName = nb.name
		}
		createdNode := buildNode(nb, nodeName, imageName, nb.osRelease, hostConfig,
			/*containerVolumes,*/ exposedPorts, envList, *nb.application)
		log.Printf("build node %s finished\n", nodeName)
		nb.cluster.nodeMap[nodeName] = createdNode
		res = append(res, createdNode)
	}
	return res
}

// create container in docker
func buildNode(nb *NodeBuilder, name string, imageName string, osRelease Release, hostConfig container.HostConfig,
	/* containerVolumes,*/ exposedPorts nat.PortSet,
	envList []string, application Application) Node {
	dockerClient := nb.cluster.dockerClient

	// ensure that there is no node with the same name
	nb.cluster.removeContainerByName(name)

	// create container by OS image
	// keep stdin open and get a tty, otherwise the container will exist at once.
	// it is the same with the 'docker create' options '-i -t' respectively.
	config := container.Config{
		ExposedPorts: exposedPorts,
		Tty:          true,
		OpenStdin:    true,
		Env:          envList,
		Cmd:          strslice.StrSlice{"/bin/bash"},
		Image:        imageName,
	}
	// configure network when create
	networkingConfig := network.NetworkingConfig{
		EndpointsConfig: map[string]*network.EndpointSettings{
			nb.cluster.network.Name: {
				NetworkID: nb.cluster.network.ID,
			},
		},
	}
	createdBody, err := dockerClient.ContainerCreate(defaultCtx, &config, &hostConfig,
		&networkingConfig, nil, name)
	if err != nil {
		panic(err)
	}
	log.Printf("Create container[%s] and port[%s] and volume[nil] and env[%s]",
		name, nb.portBindingsStr /*containerVolumes.toString(),*/, envList)
	containerID := createdBody.ID

	err = dockerClient.ContainerStart(defaultCtx, containerID, types.ContainerStartOptions{})
	if err != nil {
		panic(err)
	}
	if len(createdBody.Warnings) != 0 {
		log.Println("Warnings from docker daemon:")
		for _, warn := range createdBody.Warnings {
			log.Println(warn)
		}
	}

	// get the running container
	containers, err := dockerClient.ContainerList(defaultCtx, types.ContainerListOptions{
		Filters: filters.NewArgs(
			filters.KeyValuePair{
				Key:   "name",
				Value: name,
			},
		),
	})
	if err != nil {
		panic(err)
	}
	if len(containers) == 0 {
		panic(errors.New("Failed to create container"))
	}

	created := containers[0]
	log.Printf("Creat container [%v] success.\n", containerID)
	dockerClient = dockerClientFactory.getDockerClient()
	os := osFactory.getContainerOS(osRelease, created, dockerClient)

	application.setOS(os)
	application.setup()
	return NewNode(name, os, created, application)
}

func (nb *NodeBuilder) ensureImageExists(imageName string) bool {
	if nb.imageExists(imageName) {
		return true
	}
	log.Println("Image [" + imageName + "] not found, try to pull it from registry.")
	defer func() {
		if r := recover(); r != nil {
			log.Println("Process is interrupted while pulling image [" + imageName + "]")
		}
	}()
	imagePullOptions := types.ImagePullOptions{
		All:           false,
		RegistryAuth:  "",
		PrivilegeFunc: nil,
		Platform:      "",
	}
	nb.cluster.dockerClient.ImagePull(defaultCtx, imageName, imagePullOptions)
	return nb.imageExists(imageName)
}

func (nb *NodeBuilder) imageExists(imageName string) bool {
	imageListOptions := types.ImageListOptions{
		All: true,
	}
	imagesSummary, err := nb.cluster.dockerClient.ImageList(defaultCtx, imageListOptions)
	if err != nil {
		panic(err)
	}
	for _, imageSummary := range imagesSummary {
		for _, tag := range imageSummary.RepoTags {
			if tag == imageName {
				return true
			}
		}
	}
	return false
}

func (nb *NodeBuilder) build() Node {
	return nb.multiBuild(1)[0]
}
