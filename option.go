package main

import (
	"flag"
)

var options = struct {
	target string
	node   int
	op     int
}{}

// Initialize command line parsing
func init() {
	flag.StringVar(&options.target, "target", "", "application to be tested")
	flag.IntVar(&options.node, "node", 0, "number of nodes")
	flag.IntVar(&options.op, "op", 0, "number of operations to be generated")
}
