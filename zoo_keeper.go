package main

import (
	"fmt"
)

type ZooKeeper struct {
	name      string
	version   string
	os        OS
	nodeCount int
	nodeId    int
}

type ZooKeeperPointer struct {
	*ZooKeeper
}

func NewZooKeeper(version string, nodeCount int, nodeId int) ZooKeeperPointer {
	return ZooKeeperPointer{&ZooKeeper{
		name:      "ZooKeeper",
		version:   version,
		nodeCount: nodeCount,
		nodeId:    nodeId,
	}}
}

func (z ZooKeeperPointer) getName() string {
	return z.name
}

func (z ZooKeeperPointer) getVersion() string {
	return z.version
}

func (z ZooKeeperPointer) setOS(os OS) {
	z.os = os
}

func (z ZooKeeperPointer) getOS() OS {
	return z.os
}

func (z ZooKeeperPointer) setup() {
	// install JDK 8
	z.os.installJDK8()
	// wget zookeeper package
	distUrl := "https://archive.apache.org/dist/zookeeper/zookeeper-" +
		z.getVersion() + "/apache-zookeeper-" + z.getVersion() + "-bin.tar.gz"
	tmpPath := "/tmp/zookeeper.tar.gz"
	z.os.wget(tmpPath, distUrl)
	// untargz zookeeper package
	zookeeperHome := "/opt/zookeeper"
	z.os.mkdir(zookeeperHome)
	z.os.untargz(tmpPath, zookeeperHome)
	// configure zookeeper
	z.os.cp(zookeeperHome+"/conf/zoo_sample.cfg", zookeeperHome+"/conf/zoo.cfg")
	zookeeperDataDir := "/tmp/zookeeper"
	z.os.mkdir(zookeeperDataDir)
	// when node count > 1, configure ID File for every node
	if z.nodeCount > 1 {
		zookeeperIdFile := zookeeperDataDir + "/myid"
		z.os.touch(zookeeperIdFile)
		z.os.fileWrite(zookeeperIdFile, fmt.Sprint(z.nodeId))
		for i := 1; i <= z.nodeCount; i++ {
			line := fmt.Sprintf("server.%d=n%d:2888:3888\n", i, i)
			z.os.fileAppendLine(zookeeperHome+"/conf/zoo.cfg", line)
		}
	}
	// start zookeeper node
	z.os.execBackground(zookeeperHome+"/bin/zkServer.sh", "start")
}
