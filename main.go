package main

import (
	"flag"
	"fmt"
	"os"
)

func main() {
	flag.Parse()
	fmt.Printf("%#v\n", options)
	if options.node < 3 || options.node > 10 {
		fmt.Println("Node count should between 3 and 10")
		os.Exit(0)
	}
	if options.op < 20 || options.op > 200 {
		fmt.Println("Operation count should between 20 and 200")
		os.Exit(0)
	}
	if options.target == "zookeeper" {
		runZooKeeperTests("127.0.0.1", "zoo",
			options.node, "centos", options.op)
	} else if options.target == "etcd" {
		runEtcdTests("127.0.0.1", "etcd",
			options.node, "centos", options.op)
	} else {
		fmt.Println("Wrong target application")
	}
}
