package main

import (
	"fmt"
)

type RegisterValue struct {
	isCall bool
	isRead bool
	val    int
}

func (r RegisterValue) String() string {
	if r.isRead {
		if r.isCall {
			return "read"
		} else {
			return fmt.Sprintf("%d", r.val)
		}
	} else {
		if r.isCall {
			return fmt.Sprintf("write(%v)", r.val)
		} else {
			return " "
		}
	}
}

type RegisterState struct {
	val int
}
