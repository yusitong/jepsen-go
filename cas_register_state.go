package main

import "fmt"

type CASRegisterState struct {
	val int
}

func (c CASRegisterState) String() string {
	return fmt.Sprint(c.val)
}
