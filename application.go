package main

// Application test application
type Application interface {
	getName() string
	getVersion() string
	setOS(os OS)
	getOS() OS
	setup()
}
