package main

import (
	"errors"
	"log"
	"math/rand"
	"reflect"
	"strings"
	"sync"
)

type Generator struct {
	clients []Client
	opMap   map[string]reflect.Method
	randoms []*rand.Rand
}

func NewGenerator(clients ...Client) (this Generator, err error) {
	if clients == nil || len(clients) < 1 {
		return Generator{}, errors.New("Need at least one client for test.")
	}
	this.clients = clients
	this.opMap = make(map[string]reflect.Method)
	// get all method and find Operation
	clientMethodNumber := reflect.TypeOf(clients[0]).NumMethod()
	for i := 0; i < clientMethodNumber; i++ {
		method := reflect.TypeOf(clients[0]).Method(i)
		if strings.HasPrefix(method.Name, "Operation") {
			opName := method.Name[len("Operation"):]
			this.opMap[opName] = method
		}
	}
	log.Println("open clients")
	this.randoms = make([]*rand.Rand, len(clients))
	for i := 0; i < len(clients); i++ {
		this.randoms[i] = NewRand()
		clients[i].open()
		log.Printf("open client no.%d finished", i)
	}
	// setup initial value
	clients[0].setup()
	log.Println("clients setup finished")
	return
}

func (g *Generator) gen(opCount int, opList ...string) (history []Record) {
	// Many threads write this list in the same time, use thread-safety collection.
	var ops []reflect.Method
	for _, op := range opList {
		if method, ok := g.opMap[op]; ok {
			ops = append(ops, method)
		}
	}
	opsLen := len(ops)
	cliCount := len(g.clients)
	historyChan := make(chan Record, cliCount*opCount*2)
	latch := sync.WaitGroup{}
	latch.Add(cliCount)
	log.Println("gen start record invoke")
	for i := 0; i < cliCount; i++ {
		go func(idx int) {
			log.Printf("idx: %d\n", idx)
			for j := 0; j < opCount; j++ {
				func() {
					log.Printf(" j: %d\n", j)
					defer func() {
						if r := recover(); r != nil {
							log.Fatalln("func (g *Generator) gen reflect panic: ", r)
						}
					}()

					method := ops[g.randoms[idx].Intn(opsLen)]
					recordValue := reflect.ValueOf(g.clients[idx]).MethodByName(method.Name).Call(nil)[0]
					record := recordValue.Interface().(Record)
					record.setPid(idx)
					historyChan <- record
					// TODO 0 should be idx
					record = g.clients[0].invoke(record)
					historyChan <- record
				}()
			}
			latch.Done()
		}(i)
	}
	log.Println("latch wait before")
	latch.Wait()
	log.Println("latch wait after")

	length := len(historyChan)

	log.Printf("length %d\n", length)
	history = make([]Record, length)
	for i := 0; i < length; i++ {
		history[i] = <-historyChan
	}
	log.Printf("history %+v\n", history)

	g.clients[0].teardown()

	for _, client := range g.clients {
		client.close()
	}
	return
}
