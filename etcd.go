package main

import (
	"fmt"
	"log"
	"strings"
)

type Etcd struct {
	name      string
	version   string
	os        OS
	nodeCount int
	nodeId    int
}

type EtcdPointer struct {
	*Etcd
}

func NewEtcd(version string, nodeCount int, nodeId int) EtcdPointer {
	return EtcdPointer{&Etcd{
		name:      "Etcd",
		version:   version,
		nodeCount: nodeCount,
		nodeId:    nodeId,
	}}
}

func (e EtcdPointer) getName() string {
	return e.name
}

func (e EtcdPointer) getVersion() string {
	return e.version
}

func (e EtcdPointer) setOS(os OS) {
	e.os = os
}

func (e EtcdPointer) getOS() OS {
	return e.os
}

func (e EtcdPointer) setup() {
	distUrl := fmt.Sprintf("https://mirrors.huaweicloud.com/etcd/%s/etcd-%s-linux-amd64.tar.gz",
		e.getVersion(), e.getVersion())
	tmpPath := "/tmp/etcd.tar.gz"
	e.os.wget(tmpPath, distUrl)
	etcdHome := "/opt/etcd"
	e.os.mkdir(etcdHome)
	e.os.untargz(tmpPath, etcdHome)
	this_name := fmt.Sprintf("n%d", e.nodeId)
	var sb strings.Builder
	for i := 1; i <= e.nodeCount; i++ {
		if i != 1 {
			sb.WriteString(",")
		}
		sb.WriteString(fmt.Sprintf("n%d=http://n%d:2380", i, i))
	}
	// n1=http://n1:2380,n2=http://n2:2380,n3=http://n3:2380,n4=http://n4:2380,n5=http://n5:2380
	e.os.execBackground("/opt/etcd/etcd", "--name", this_name,
		"--initial-advertise-peer-urls", "http://"+this_name+":2380",
		"--listen-peer-urls", "http://0.0.0.0:2380",
		"--advertise-client-urls", "http://0.0.0.0:2379",
		"--listen-client-urls", "http://0.0.0.0:2379",
		"--initial-cluster", sb.String(),
		"--initial-cluster-state", "new",
		"--initial-cluster-token", "my-etcd-token")
	log.Println("etcd execBackground")
}
