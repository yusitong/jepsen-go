package main

import (
	"testing"
)

func TestZooKeeper(t *testing.T) {
	runZooKeeperTests("127.0.0.1", "zoo",
		3, "centos", 20)
}

func TestEtcd(t *testing.T) {
	runEtcdTests("127.0.0.1", "etcd",
		3, "centos", 20)
}
