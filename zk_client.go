package main

import (
	"bytes"
	"fmt"
	"github.com/go-zookeeper/zk"
	"log"
	"math/rand"
	"time"
)

type ZKClient struct {
	host        string
	port        int
	testKey     string
	rand        *rand.Rand
	zkConn      *zk.Conn
	zkEventChan <-chan zk.Event
}

type ZKClientPointer struct {
	*ZKClient
}

func NewZKClient(host string, port int) ZKClientPointer {
	return ZKClientPointer{&ZKClient{
		host:    host,
		port:    port,
		testKey: "/4-test-key",
		rand:    NewRand(),
	}}
}

func (z ZKClientPointer) open() {
	log.Println("zookeeper client open start")
	connectStr := fmt.Sprintf("%s:%d", z.host, z.port)

	log.Println("new zookeeper")
	var err error
	z.zkConn, z.zkEventChan, err = zk.Connect([]string{connectStr}, time.Second*5)
	if err != nil {
		panic(err)
	}
	select {
	case event := <-z.zkEventChan:
		log.Println(event)
		break
	case <-time.After(time.Second * 5):
		log.Println("zkclient event time out")
	}
}

func (z ZKClientPointer) setup() {
	_, err := z.zkConn.Create(z.testKey, []byte("0"), 0, zk.WorldACL(zk.PermAll))
	if err != nil {
		panic(err)
	}
}

func (z ZKClientPointer) invoke(record Record) Record {
	switch record.func_ {
	case "read":
		byteSlice, _, err := z.zkConn.Get(z.testKey)
		if err != nil {
			log.Println(err)
			return record.Fail()
		}
		return record.Ok(string(byteSlice))
	case "write":
		val := []byte(record.args[0])
		_, err := z.zkConn.Set(z.testKey, val, -1)
		if err != nil {
			log.Println(err)
			return record.Fail()
		}
		return record.Ok()
	case "cas":
		oldVal := []byte(record.args[0])
		newVal := []byte(record.args[1])
		byteSlice, _, err := z.zkConn.Get(z.testKey)
		if err != nil {
			log.Println(err)
			return record.Fail()
		}
		if bytes.Compare(byteSlice, oldVal) != 0 {
			return record.Fail()
		}
		_, err = z.zkConn.Set(z.testKey, newVal, -1)
		if err != nil {
			log.Println(err)
			return record.Fail()
		}
		return record.Ok()
	}
	panic("Unknown operation name")
}

func (z ZKClientPointer) teardown() {
	err := z.zkConn.Delete(z.testKey, -1)
	if err != nil {
		log.Println(err)
	}
}

func (z ZKClientPointer) close() {
	defer func() {
		if r := recover(); r != nil {
			log.Printf("%#v\n", r)
		}
	}()
	z.zkConn.Close()
}

// Operation

func (z ZKClientPointer) OperationRead() Record {
	return NewRecordWithFuncAndArgs("read")
}
func (z ZKClientPointer) OperationWrite() Record {
	x := fmt.Sprint(z.rand.Intn(5))
	return NewRecordWithFuncAndArgs("write", x)
}
