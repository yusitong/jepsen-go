package main

import (
	"errors"
	"fmt"
	"github.com/anishathalye/porcupine"
	"log"
	"time"
)

func runEtcdTests(host string, clusterName string, node int, image string, op int) {
	release := CentOSTag
	tag := "8"
	if image == string(UbuntuTag) {
		//release = Release.Ubuntu
		//tag = "20.04"
		log.Fatalln("Ubuntu is not support currently, use centos instead.")
	}
	// cluster
	cluster := NewCluster(clusterName)
	var clients = make([]Client, node)
	var ports = make([]int, node)
	for i := 1; i <= node; i++ {
		var port = 12380 + i
		log.Printf("startup no.%d node\n", i)
		etcd := Application(NewEtcd("v3.4.13", node, i))
		nodeBuilder := NodeBuilder{
			cluster:           &cluster,
			osRelease:         release,
			imageTag:          tag,
			name:              fmt.Sprintf("n%d", i),
			portBindingsStr:   fmt.Sprintf("%d:2379/tcp", port),
			volumeBindingsStr: "",
			envVariablesStr:   "",
			application:       &etcd,
		}
		_ = nodeBuilder.build()
		ports[i-1] = port
	}
	for i := 0; i < node; i++ {
		clients[i] = NewEtcdClient(host, ports[i])
	}
	log.Println("set generator")
	generator, _ := NewGenerator(clients...)
	log.Println("generator gen recordList")
	recordList := generator.gen(op, "Read", "Write", "CAS")
	log.Println("generator gen recordList finished")

	var history []porcupine.Event
	var renumber = make(map[int]int)
	var id = 0
	var recordOrders = make(map[string]int)
	for i, record := range recordList {
		if record.type_ == INVOKE {
			value := CASRegisterValue{
				isCall: true,
				out:    0,
			}
			switch record.func_ {
			case "read":
				value = CASRegisterValue{
					isCall: true,
					op:     CASRegisterValueOP_R,
				}
			case "write":
				value = CASRegisterValue{
					isCall: true,
					op:     CASRegisterValueOP_W,
					in1:    MustAToI(record.args[0]),
				}
			case "cas":
				value = CASRegisterValue{
					isCall: true,
					op:     CASRegisterValueOP_C,
					in1:    MustAToI(record.args[0]),
					in2:    MustAToI(record.args[1]),
				}
			default:
				panic(errors.New("etcd record func error"))
			}
			renumber[i] = id
			history = append(history, porcupine.Event{
				ClientId: record.pid,
				Kind:     porcupine.CallEvent,
				Value:    value,
				Id:       id,
			})
			recordOrders[fmt.Sprintf("%#v", record)] = id
			id++
		} else if record.type_ == OK {
			value := CASRegisterValue{
				isCall: false,
				in1:    0,
				in2:    0,
			}
			switch record.func_ {
			case "read":
				value = CASRegisterValue{
					isCall: false,
					op:     CASRegisterValueOP_R,
					out:    MustAToI(record.args[0]),
				}
			case "write":
				value = CASRegisterValue{
					isCall: false,
					op:     CASRegisterValueOP_W,
					out:    0,
				}
			case "cas":
				value = CASRegisterValue{
					isCall: false,
					op:     CASRegisterValueOP_C,
					out:    MustAToI(record.args[2]),
				}
			default:
				panic(errors.New("etcd record func error"))
			}
			history = append(history, porcupine.Event{
				ClientId: record.pid,
				Kind:     porcupine.ReturnEvent,
				Value:    value,
				Id:       recordOrders[fmt.Sprintf("%#v", *record.prev)],
			})
		} else {
			panic(errors.New("etcd record type error"))
		}
	}
	log.Printf("finish history count %#v", history)

	casRegisterModelInit := func() interface{} {
		return CASRegisterState{val: 0}
	}
	casRegisterModelStep := func(stateInter interface{}, inputInter interface{}, outputInter interface{}) (bool, interface{}) {
		oldState := stateInter.(CASRegisterState)
		input := inputInter.(CASRegisterValue)
		output := outputInter.(CASRegisterValue)
		switch input.op {
		case CASRegisterValueOP_R:
			if output.out != oldState.val {
				return false, nil
			}
			return true, CASRegisterState{oldState.val}
		case CASRegisterValueOP_W:
			return true, CASRegisterState{input.in1}
		case CASRegisterValueOP_C:
			if oldState.val == input.in1 {
				if output.out != input.in2 {
					return false, nil
				}
				return true, CASRegisterState{input.in2}
			}
			if output.out != oldState.val {
				return false, nil
			}
			return true, CASRegisterState{oldState.val}
		}
		return false, nil
	}
	casRegisterModel := porcupine.Model{
		Partition:         nil,
		PartitionEvent:    nil,
		Init:              casRegisterModelInit,
		Step:              casRegisterModelStep,
		Equal:             nil,
		DescribeOperation: nil,
		DescribeState:     nil,
	}
	checkResult, linearizationInfo := porcupine.CheckEventsVerbose(casRegisterModel, history, 5*time.Minute)
	log.Println("checkResult: ", checkResult)
	log.Printf("linearizationInfo: %+v\n", linearizationInfo)
	if err := porcupine.VisualizePath(casRegisterModel, linearizationInfo, "result.html"); err != nil {
		panic(err)
	}
}
