package main
// TODO use

type NemesisType int

const (
	ClockSkew = NemesisType(iota)
	DiskBite
	NetworkPartition
	NodeCrash
	TrafficCongestion
)
