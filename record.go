package main

import (
	"fmt"
	"strings"
)

type RecordType int

const (
	INVOKE = RecordType(iota)
	OK
	FAIL
	INFO
	INJECT
	HEAL
)

type Record struct {
	// process id
	pid        int
	type_      RecordType
	nemesis    NemesisType
	func_      string
	args       []string
	info       string
	prev, next *Record
}

func NewRecordWithFuncAndArgs(func_ string, args ...string) Record {
	return Record{
		type_: INVOKE,
		func_: func_,
		args:  args,
	}
}

func NewRecordWithNemesis(nemesisType NemesisType) Record {
	return Record{
		type_:   INJECT,
		nemesis: nemesisType,
	}
}

func (r *Record) setPid(pid int) {
	r.pid = pid
}

func (r *Record) Ok(results ...string) Record {
	res := r.nextEvent()
	res.type_ = OK
	if results != nil {
		res.args = append(res.args, results...)
	}
	return res
}

func (r *Record) Fail() Record {
	res := r.nextEvent()
	res.type_ = FAIL
	return res
}

func (r *Record) Info(info string) Record {
	res := r.nextEvent()
	res.type_ = INFO
	res.info = info
	return res
}

func (r *Record) Heal() Record {
	res := r.nextEvent()
	res.type_ = HEAL
	return res
}

func (r *Record) nextEvent() Record {
	nr := *r
	nr.prev = r
	r.next = r.prev
	return nr
}

func (r Record) String() string {
	res := ""
	argsStr := ""
	if len(r.args) > 0 {
		argsStr = strings.Join(r.args, " ")
	}
	switch r.type_ {
	case INVOKE, OK, FAIL, INFO:
		res = fmt.Sprintf("%3d %6d %s %s", r.pid, r.type_, r.func_, argsStr)
		if r.type_ == INFO {
			res += " " + r.info
		}
	case INJECT, HEAL:
		res = fmt.Sprintf("%3d %6d %v %s", r.pid, r.type_, r.nemesis, argsStr)
	}
	return res
}
